#! /usr/bin/env perl
# -*-Perl-*-
#
#ident	"@(#)ccvs/contrib:Id: log_accum,v 1.72 2003/04/06 20:49:08 perry Exp"
#
# Perl filter to handle the log messages from the checkin of files in multiple
# directories.  This script will group the lists of files by log message, and
# send one piece of mail per unique message, no matter how many files are
# committed.
#
# This implementation requires:
# 1) a pre-commit checking program that leaves a #cvs.lastdir file containing
#    the name of the last directory,
# 2) the `%{Vvts}' output format in the loginfo file (so that the version
#    numbers and tags are all passed in, and in the right order), and
# 3) perl5-MD5.
#
# Contributed by David Hampton <hampton@cisco.com>
# Hacked greatly by Greg A. Woods <woods@planix.com>
# Rewritten by Charles M. Hannum <mycroft@netbsd.org>
# Least common path routine by Hubert Feyrer <hubertf@netbsd.org>
# rdiff & bugsto support by Darrin B. Jewell <dbj@netbsd.org>
# Slightly modified for KLDP.net by Bang Jun-Young <junyoung@users.kldp.net>

# Usage: log_accum.pl [-d] [-D] [-S] [-M module] [[-m mailto] ...] [[-R replyto] ...] [[-b bugsto] ...] [-f logfile]
#	-d		- turn on debugging
#	-m mailto	- send mail to "mailto" (multiple)
#	-R replyto	- set the "Reply-To:" to "replyto" (multiple)
#	-M modulename	- set module name to "modulename"
#	-f logfile	- write commit messages to logfile too
#	-D		- generate diff commands
#	-S		- write to "syncer" file in /tmp used by push script
#	-b bugsto	- send mail referencing problem reports to this address

use MD5;

#
#	Configurable options
#

# set this to something that takes a whole message on stdin
@MAILER	       = ("/usr/lib/sendmail", "-t");

#
#	End user configurable options.
#

$LASTDIR_FILE = "/tmp/#cvs.lastdir";
$HASH_FILE = "/tmp/#cvs.hash";
$VERSION_FILE = "/tmp/#cvs.version";
$MESSAGE_FILE = "/tmp/#cvs.message";
$MAIL_FILE = "/tmp/#cvs.mail";
$SYNCER_FILE= "/tmp/cvs_commits";

#
#	Subroutines
#

#
# An O(n) routine to find the least common directory in a number
# of paths  - Hubert Feyrer <hubertf@netbsd.org>
#
# Beware: paths are anon-arrays of [0, 1, 2, 3, "path"] !
#
sub lcpath
{
    local(@paths) = @_;
    local($lcpathlen, @lcpath, $i, @c, $dir, $ref, $firstpath);
    
    $lcpathlen=0;
    @lcpath[0..999] = "/";		# "/" => never set, "" => truncated
    $firstpath = 1;

    for($i=0; $i<100; $i++) { $lcpath[$i] = "/"; }
    
    foreach $ref (@paths){
	$dir = $$ref[4];
	print("HF: dir=$dir lcpath=",
	      join("/", @lcpath[0..$lcpathlen]), " ($lcpathlen)\n")
	    if $debug;
	
	@c = split(/\//, $dir);
      component:
	for($i=0; $i <= $#c; $i++) {
	    print("$i: $c[$i]	lc=$lcpath[$i]" .
		  "      lcpath=", join("/", @lcpath[0..$lcpathlen-1]),"\n")
		if $debug;
	    
	    if ($lcpath[$i] eq "/") {
		if ($firstpath) {
		    # never been there
		    $lcpath[$i] = $c[$i];
		    $lcpathlen = $i;
		    print "-> $c[$i] added at $i\n"
			if $debug;
		} else {
		    # Something was here before - stop!
		    print "-> stopped by earlier shorter path\n"
			if $debug;
		    last component;
		}
	    } else {
		if ($c[$i] ne $lcpath[$i]) {
		    # different names 
		    $lcpath[$i] = "";
		    $lcpathlen = $i-1;
		    print "-> truncated at $lcpathlen ($c[$i])\n"
			if $debug;
		    last component;
		}
	    }
	}
	
	if ($lcpathlen > $#c and $lcpathlen > 0) {
	    $lcpathlen = $#c;
	    $lcpath[$#c + 1] = "";
	    print "-> truncated at $#c\n"
		if $debug;
	}

	$firstpath = 0
	    if $firstpath;
	
	print "\n"
	    if $debug;
    }	
    
    print "lcpath = ", join("/", @lcpath[0..$lcpathlen]), " ($lcpathlen)\n"
	if $debug;
    
    return join("/", @lcpath[0..$lcpathlen]);
}

sub append_logfile {
    local($filename, @lines) = @_;
    local($_);

    open(FILE, ">>$filename") || die("Cannot open file $filename for append.\n");
    foreach (@lines) {
	print FILE $_."\n";
    }
    close(FILE);
}

sub write_logfile {
    local($filename, @lines) = @_;
    local($_);

    open(FILE, ">$filename") || die("Cannot open file $filename for write.\n");
    foreach (@lines) {
	print FILE $_."\n";
    }
    close(FILE);
}

sub read_logfile {
    local($filename) = @_;
    local(@lines);

    open(FILE, "<$filename") || die("Cannot open file $filename for read.\n");
    while (<FILE>) {
	chop;
	push @lines, $_;
    }
    close(FILE);

    @lines;
}

sub format_lists {
    local(@lines, $line, $_, $last, $f);

    if ($debug) {
	print STDERR "format_lists(): files = ", join(" ", @_), ".\n";
    }

    # Sort by tag, dir, file.
    @_ = sort {
	$$a[2] cmp $$b[2] ||
	$$a[4] cmp $$b[4] ||
	$$a[3] cmp $$b[3];
    } @_;

    # Combine adjacent rows that are the same modulo the file name.
    @_ = map {
	if (!$last || $$_[2] ne $$last[2] || $$_[4] ne $$last[4]) {
	    $last = [@$_[0..2], [$$_[3]], @$_[4]];
	    $last;
	} else {
	    push @{$$last[3]}, $$_[3];
	    ();
	}
    } @_;

    foreach (@_) {
	$line = "\t".$$_[4];
	$line .= " [".$$_[2]."]" if $$_[2];
	$branches{$$_[2] ne ""? $$_[2] : "trunk" } = 1;
	$line .= ":";
	foreach $f (@{$$_[3]}) {
	    if (length($line) + length($f) > 71) {
		push(@lines, $line);
		$line = "\t   ";
	    }
	    $line .= " ".$f;
	}
	push @lines, $line;
    }

    @lines;
}

sub format_diffs {
    local(@lines, $line, $_, $last, $f);

    if ($debug) {
	print STDERR "format_diffs(): files = ", join(" ", @_), ".\n";
    }

    # Sort by dir, old, new, file.
    @_ = sort {
	$$a[4] cmp $$b[4] ||
	$$a[0] cmp $$b[0] ||
	$$a[1] cmp $$b[1] ||
	$$a[3] cmp $$b[3];
    } @_;

    # Combine adjacent rows that are the same modulo the file name.
    @_ = map {
	if (!$last || $$_[4] ne $$last[4] || $$_[0] ne $$last[0] ||
		      $$_[1] ne $$last[1]) {
	    $last = [@$_[0..2], [$$_[3]], @$_[4]];
	    $last;
	} else {
	    push @{$$last[3]}, $$_[3];
	    ();
	}
    } @_;

    # Sort by dir, file.
    @_ = sort {
	$$a[4] cmp $$b[4] ||
	$$a[3][0] cmp $$b[3][0];
    } @_;

    foreach (@_) {
	$line = "cvs rdiff -r$$_[0] -r$$_[1]";
	foreach $f (@{$$_[3]}) {
	    if (length($line) + length($$_[4]."/".$f) > 76) {
		push @lines, $line." \\";
		$line = "   ";
	    }
	    $line .= " ".$$_[4]."/".$f;
	}
	push @lines, $line;
    }

    @lines;
}

sub build_header {
    local($header, $now);
    $now = gmtime;
    $header = sprintf("Module Name:\t%s\nCommitted By:\t%s\nDate:\t\t%s %s %s",
		      $modulename,
		      $login,
		      substr($now, 0, 19), "UTC", substr($now, 20, 4));
}

# Search text for possible references to gnats prs
sub search_for_prs {
    local(@text) = @_;
    local(@prs);
    local($last);
    @prs = ();
    foreach $_ (@text) {
        while (m/\bP(roblem)?\s*?R(eport)?\s*\/?\#?\s*(\b\w+\/)?(\d+)\b/igo) {
            push @prs,$4;
        }
    }
    @prs = map { if (!$last || $last ne $_) { $last = $_ ; } else { () } } sort @prs;

    @prs;
}

sub mail_notification {
    local(@text) = @_;
    local($branches, $s, $_, $name);
    local($subject, $lcpath);

    # prepend any branches that we might have ...
    #
    $branches = join(", ", sort(keys(%branches)));
    if ($branches ne "" and $branches ne "trunk") {	# branch commit!
        $subject = "[$branches] ";
    } else {
	$subject = "";
    }

    # ... and add least common path component of all dirs
    #
    $lcpath = lcpath(@added_files, @removed_files, @modified_files);
    if ($lcpath ne "/" and $lcpath ne "") {
	$subject .= $lcpath;		# cvs commit
	if ($do_syncer) {
		&append_logfile($SYNCER_FILE, $lcpath);
	}
    } else {
	$subject .= "$dir";		# cvs import
	if ($do_syncer) {
		&append_logfile($SYNCER_FILE, "$dir");
	}
    }
	
    # Fetch the user's full name from the GECOS field.  We have to do the
    # magic & substitution, and possibly quote it for RFC822 as well.
    #
    $_ = (split(",", (getpwnam($login))[6]))[0];
    s,&,\u$login,g;
    if (m,[^- !#-'*+/-9=?A-Z^-~],) {
	s,[\"\\],\\$&,;
	$_ = "\"$_\"";
    }
    $name = $_;

    &write_logfile("$MAIL_FILE.$id",
        "From: " . $login."\@users.kldp.net",
        "Subject: CVS commit: $subject",
        "To: " . $mailto,
        "Reply-To: " . $replyto."\@users.kldp.net",
        "Content-Type: text/plain; charset=euc-kr",
        "",
        "",
        @text);
    unless ($pid = fork) {
	open(STDIN, "<$MAIL_FILE.$id") || die("Cannot open file $filename for read.\n");
	exec(@MAILER) || die("Cannot exec @MAILER.\n");
    }
    waitpid($pid, 0);


    if ($bugsto) {
        local(@prs);
        @prs = &search_for_prs(@text);
        foreach $pr (@prs) {
            &write_logfile("$MAIL_FILE.$id",
                           "From: " . ($name ? $name." <".$login.">" : $login),
                           "Subject: pr/$pr CVS commit: $subject",
                           "To: " . $bugsto,
                           "Reply-To: " . $replyto,
                           "",
                           @text);
            unless ($pid = fork) {
                open(STDIN, "<$MAIL_FILE.$id") || die("Cannot open file $filename for read.\n");
                exec(@MAILER) || die("Cannot exec @MAILER.\n");
            }
            waitpid($pid, 0);
        }
    }
}

#
#	Main Body
#

# Initialize basic variables
#
$debug = 0;
$id = getpgrp();	# note, you *must* use a shell which does setpgrp()
($login) = getpwuid($<);
$login || die "*** Who are you?";
$do_diff = 0;

# parse command line arguments (file list is seen as one arg)
#
while (@ARGV) {
    $_ = shift @ARGV;
    if ($_ eq '-d') {
	$debug = 1;
	print STDERR "Debug turned on...\n";
    } elsif ($_ eq '-m') {
	$mailto .= ", " if $mailto;
	$mailto .= shift @ARGV;
    } elsif ($_ eq '-b') {
	$bugsto .= ", " if $bugsto;
	$bugsto .= shift @ARGV;
    } elsif ($_ eq '-R') {
	$replyto .= ", " if $replyto;
	$replyto .= shift @ARGV;
    } elsif ($_ eq '-M') {
	die("too many '-M' args\n") if $modulename;
	$modulename = shift @ARGV;
    } elsif ($_ eq '-f') {
	die("too many '-f' args\n") if $commitlog;
	$commitlog = shift @ARGV;
	# This is a disgusting hack to untaint $commitlog if we're running from
	# setgid cvs.
	$commitlog =~ m/(.*)/;
	$commitlog = $1;
    } elsif ($_ eq '-D') {
	$do_diff = 1;
    } elsif ($_ eq '-S') {
	$do_syncer = 1;
    } else {
	@files = split;
	last;
    }
}
if (@ARGV) {
    die("Too many arguments!  Check usage.\n");
}

if (! $mailto) {
    die("No mail recipient specified (use -m)\n");
}
if (! $replyto) {
    $replyto = $login;
}

# for now, the first "file" is the repository directory being committed,
# relative to the $CVSROOT location
#
$dir = shift @files;

# XXX there are some ugly assumptions in here about module names and
# XXX directories relative to the $CVSROOT location -- really should
# XXX read $CVSROOT/CVSROOT/modules, but that's not so easy to do, since
# XXX we have to parse it backwards.
#
# XXX For now we set the `module' name to the top-level directory name.
#
if (! $modulename) {
    ($modulename) = split('/', $dir, 2);
}

if ($debug) {
    print STDERR "module - ", $modulename, "\n";
    print STDERR "dir    - ", $dir, "\n";
    print STDERR "files  - ", join(" ", @files), "\n";
    print STDERR "id     - ", $id, "\n";
}

# Check for a new directory or an import command.
#
#    files[0] - "-"
#    files[1] - "New"
#    files[2] - "directory"
#
#    files[0] - "-"
#    files[1] - "Imported"
#    files[2] - "sources"
#
if ($files[0] eq "-") {
    if ($files[1] eq "New" && $files[2] eq "directory") {
	# Forget about it
    } else {
	local(@text);

	@text = ();
	push @text, &build_header();
	push @text, "";

	while (<STDIN>) {
	    chop;			# Drop the newline
	    push @text, $_;
	}

	# Write to the commitlog file
	#
	if ($commitlog) {
	    &append_logfile($commitlog, @text);
	}

	# Mail out the notification.
	#
	&mail_notification(@text);
    }

    exit 0;
}

if ($debug) {
    print STDERR "files  - ", join(" ", @files), "\n";
}

# Collect just the log message from stdin.
#
while (<STDIN>) {
    chop;			# strip the newline
    last if (/^Log Message:$/);
}
while (<STDIN>) {
    chop;			# strip the newline
    s/\s+$//;			# strip trailing white space
    push @log_lines, $_;
}

$md5 = MD5->new();
foreach (@log_lines) {
    $md5->add($_."\n");
}
$hash = $md5->hexdigest();
undef $md5;

if ($debug) {
    print STDERR "hash = $hash\n";
}
if (! -e "$MESSAGE_FILE.$id.$hash") {
    &append_logfile("$HASH_FILE.$id", $hash);
    &write_logfile("$MESSAGE_FILE.$id.$hash", @log_lines);
}
    
# Spit out the information gathered in this pass.
#
&append_logfile("$VERSION_FILE.$id.$hash", $dir.'/', @files);

# Check whether this is the last directory.  If not, quit.
#
if ($debug) {
    print STDERR "Checking current dir against last dir.\n";
}
($_) = &read_logfile("$LASTDIR_FILE.$id");

if ($_ ne $dir) {
    if ($debug) {
	print STDERR sprintf("Current directory %s is not last directory %s.\n", $dir, $_);
    }
    exit 0;
}
if ($debug) {
    print STDERR sprintf("Current directory %s is last directory %s -- all commits done.\n", $dir, $_);
}

#
#	End Of Commits!
#

# This is it.  The commits are all finished.  Lump everything together
# into a single message, fire a copy off to the mailing list, and drop
# it on the end of the Changes file.
#

#
# Produce the final compilation of the log messages
#

@hashes = &read_logfile("$HASH_FILE.$id");
foreach $hash (@hashes) {
    # In case we're running setgid, make sure the hash file hasn't been hacked.
    $hash =~ m/([a-z0-9]*)/ || die "*** Hacking attempt detected\n";
    $hash = $1;

    @text = ();
    push @text, &build_header();
    push @text, "";

    @files = &read_logfile("$VERSION_FILE.$id.$hash");
    @log_lines = &read_logfile("$MESSAGE_FILE.$id.$hash");

    foreach (@files) {
	if (s/\/$//) {
	    $dir = $_;
	    next;
	}
	$_ = [split(',', $_, 4), $dir];
	if ($$_[0] eq 'NONE') {
	    $$_[0] = '0';
	    push @added_files, $_;
	} elsif ($$_[1] eq 'NONE') {
	    $$_[1] = '0';
	    push @removed_files, $_;
	} else {
	    push @modified_files, $_;
	}
    }

    # Strip leading and trailing blank lines from the log message.  Also
    # compress multiple blank lines in the body of the message down to a
    # single blank line.
    #
    $blank = 1;
    @log_lines = map {local $wasblank = $blank;
		      $blank = $_ eq '';
		      $blank && $wasblank ? () : $_;} @log_lines;
    pop @log_lines if $blank;

    if (@modified_files) {
	push @text, "Modified Files:";
	push @text, &format_lists(@modified_files);
    }
    if (@added_files) {
	push @text, "Added Files:";
	push @text, &format_lists(@added_files);
    }
    if (@removed_files) {
	push @text, "Removed Files:";
	push @text, &format_lists(@removed_files);
    }
    if (@log_lines) {
	push @text, "";
	push @text, "Log Message:";
	push @text, @log_lines;
    }
    push @text, "";

    # Write to the commitlog file
    #
    if ($commitlog) {
	&append_logfile($commitlog, @text);
    }

    if ($do_diff) {
	push @text, "";
	push @text, "To generate a diff of this commit:";
	push @text, &format_diffs(@modified_files, @added_files,
				  @removed_files);
	push @text, "";
	push @text, "Please note that diffs are not public domain; they are subject to the";
	push @text, "copyright notices on the relevant files.";
	push @text, "";
    }

    # Mail out the notification.
    #
    &mail_notification(@text);

    if (! $debug) {
	unlink "$VERSION_FILE.$id.$hash";
	unlink "$MESSAGE_FILE.$id.$hash";
	unlink "$MAIL_FILE.$id";
    }
}

if (! $debug) {
    unlink "$LASTDIR_FILE.$id";
    unlink "$HASH_FILE.$id";
}

exit 0;
