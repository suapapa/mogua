/*
 * Copyright 2003
 *     Bang Jun-Young.  All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *	This product includes software developed by Bang Jun-Young.
 * 4. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef _HEAP_H
#define _HEAP_H

/*
 * Additional heap flags.
 */
#define HEAP_CREATED_VIA_HEAP_CREATE	0x00001000
#define HEAP_PAGE_DEBUG_ENABLED		0x01000000	/* XXX */
#define HEAP_USER_STACK_TRACE_DB	0x08000000
#define HEAP_DISABLE_DEBUGGING		0x10000000	/* XXX */
#define HEAP_VALIDATION_ON_CALL		0x20000000
#define HEAP_PARAMETER_CHECKING		0x40000000
#define HEAP_RETAIN_CRITICAL_SECTION	0x80000000

/*
 * Heap memory block flags.
 */
#define RTL_HEAP_BLOCK_BUSY		0x01
#define RTL_HEAP_BLOCK_EXTRA_STUFF	0x02
#define RTL_HEAP_BLOCK_DEBUG		0x04
#define RTL_HEAP_BLOCK_BIG		0x08
#define RTL_HEAP_BLOCK_FREE		0x10

/*
 * Heap flags used in RtlWalkHeap() functions.
 */
#define RTL_PROCESS_HEAP_ENTRY_BUSY		0x0001
#define RTL_PROCESS_HEAP_SEGMENT		0x0002
#define RTL_PROCESS_HEAP_UNCOMMITTED_RANGE	0x0100
#define RTL_PROCESS_HEAP_ENTRY_MOVEABLE		0x0200
#define RTL_PROCESS_HEAP_ENTRY_DDESHARE		0x0400

/*
 * Heap signatures.
 */
#define RTL_HEAP_SIGNATURE		0xEEFFEEFF
#define RTL_HEAP_SEGMENT_SIGNATURE	0xFFEEFFEE

/*
 * Flags for RtlpDisableHeapLookaside.
 */
#define RTLP_DISABLE_HEAP_LOOKASIDE	1
#define RTLP_DISABLE_HEAP_CACHE		2

/*
 * Some convenient macros.
 */
#define GET_HEAP_ENTRY(p)					\
	((PRTL_HEAP_ENTRY)(((PVOID)(p)) - 			\
	sizeof(RTL_HEAP_ENTRY)))

#define GET_NEXT_HEAP_ENTRY(p,s)				\
	((PRTL_HEAP_ENTRY)(((PVOID)(p)) +			\
	((s) * 8)))

#define GET_FREE_BLOCK(p)					\
	((PRTL_HEAP_FREE_BLOCK)(((PVOID)(p)) -			\
	sizeof(RTL_HEAP_ENTRY)))

#define GET_MEM_BLOCK(p)					\
	((PVOID)(p) + sizeof(RTL_HEAP_ENTRY))

/*
 * Internal structures used by heap functions.
 */

typedef struct _RTL_HEAP_ENTRY {
/*000*/	WORD Size;		/* Block size divided by 8 */
/*002*/	WORD PrevSize;		/* Size of previous memory block */
/*004*/	BYTE Index;		/* Index of segment the block belongs to */
/*005*/	BYTE Flags;
/*006*/	BYTE Overhead;
/*007*/	BYTE Tag;
} RTL_HEAP_ENTRY, *PRTL_HEAP_ENTRY;

typedef struct _RTL_HEAP_EXTRA_STUFF {
	WORD Unknown_000;
/*002*/	WORD Tag;
/*004*/	HANDLE Handle;
} RTL_HEAP_EXTRA_STUFF, *PRTL_HEAP_EXTRA_STUFF;

typedef struct _RTL_HEAP_BIG_BLOCK {
/*000*/	LIST_ENTRY List;
/*008*/	RTL_HEAP_EXTRA_STUFF ExtraStuff;
/*010*/	DWORD Size[2];
/*018*/	RTL_HEAP_ENTRY Entry;
} RTL_HEAP_BIG_BLOCK, *PRTL_HEAP_BIG_BLOCK;

typedef struct _RTL_HEAP_FREE_BLOCK {
/*000*/	struct _RTL_HEAP_ENTRY;
/*008*/	LIST_ENTRY List;
} RTL_HEAP_FREE_BLOCK, *PRTL_HEAP_FREE_BLOCK;

typedef struct _RTL_HEAP_UCR_SEGMENT {
/*000*/	struct _RTL_HEAP_UCR_SEGMENT *Next;
/*004*/	DWORD ReservedSize;
/*008*/	DWORD CommittedSize;
/*00C*/	DWORD Reserved;			/* Not currently used */
} RTL_HEAP_UCR_SEGMENT, *PRTL_HEAP_UCR_SEGMENT;

/*
 * Next: points to the next uncommitted range available, and is set to NULL if
 *	the current uncommitted range is allocated.
 * LastAddress: page-aligned starting address not yet allocated.
 * UnCommittedSize: difference between the starting address of the next range
 *	and LastAddress. It's always multiple of the page size.
 */
typedef struct _RTL_HEAP_UNCOMMITTED_RANGE {
/*000*/	struct _RTL_HEAP_UNCOMMITTED_RANGE *Next;
/*004*/	PVOID LastAddress;
/*008*/	DWORD UnCommittedSize;
/*00C*/	DWORD Reserved;			/* Not currently used */
} RTL_HEAP_UNCOMMITTED_RANGE, *PRTL_HEAP_UNCOMMITTED_RANGE;

typedef struct _RTL_HEAP_CACHE {
/*000*/	DWORD Entries;
/*004*/	DWORD CommittedSize;
/*008*/	LONG Depth;
/*00C*/	LONG HighDepth;
/*010*/	LONG LowDepth;
/*014*/	DWORD Sequence;
/*018*/	DWORD ExtendCount;		/* XXX */
/*01C*/	DWORD CreateUCRCount;		/* XXX */
/*020*/	LONG LargestHighDepth;
/*024*/	LONG DepthSize;			/* Largest HighDepth - LowDepth */
/*028*/	PBYTE Bitmap;
/*02C*/	PRTL_HEAP_FREE_BLOCK *FreeBlocks;
} RTL_HEAP_CACHE, *PRTL_HEAP_CACHE;

/*
 * Flags: original flags specified at heap creation time.
 * ForceFlags: can be one or more of
 *	HEAP_PARAMETER_CHECKING,
 *	HEAP_VALIDATION_ON_CALL,
 *	HEAP_CREATE_ALIGN_16,
 *	HEAP_FREE_CHECKING_ENABLED,
 *	HEAP_TAIL_CHECKING_ENABLED,
 *	HEAP_REALLOC_IN_PLACE_ONLY,
 *	HEAP_ZERO_MEMORY,
 *	HEAP_GENERATE_EXCEPTIONS,
 *	and HEAP_NO_SERIALIZE.
 */
typedef struct _RTL_HEAP {
/*000*/	struct _RTL_HEAP_ENTRY Entry;
/*008*/	DWORD Signature;
/*00C*/	DWORD Flags;
/*010*/	DWORD ForceFlags;
/*014*/	DWORD VirtualMemoryThreshold;		/* Divided by 8 */
/*018*/	DWORD SegmentReserve;
/*01C*/	DWORD SegmentCommit;
/*020*/	DWORD DeCommitFreeBlockThreshold;	/* Divided by 8 */
/*024*/	DWORD DeCommitTotalFreeThreshold;	/* Divided by 8 */
/*028*/	DWORD TotalFreeSize;			/* Divided by 8 */
/*02C*/	DWORD MaximumAllocationSize;
/*030*/	WORD ProcessHeapsListIndex;
/*032*/	WORD HeaderValidateLength;		/* Size in bytes */
/*034*/	PVOID HeaderValidateCopy;
/*038*/	WORD NextAvailableTagIndex;
/*03A*/	WORD MaximumTagIndex;
/*03C*/	PVOID TagEntries;
/*040*/	PRTL_HEAP_UCR_SEGMENT UCRSegments;
/*044*/	PRTL_HEAP_UNCOMMITTED_RANGE UnusedUnCommittedRanges;
/*048*/	DWORD AlignRound;
/*04C*/	DWORD AlignMask;
/*050*/	LIST_ENTRY VirtualAllocdBlocks;
/*058*/	struct _RTL_HEAP_SEGMENT *Segments[64];
/*158*/	BYTE FreeListsInUse[16];	/* 16 bytes * 8 bits = 128 blocks */
/*168*/	WORD FreeListsInUseTerminate;
/*16A*/	WORD AllocatorBackTraceIndex;
/*16C*/	DWORD NonDedicatedListLength;
/*170*/	PRTL_HEAP_CACHE Cache;
/*174*/	PVOID PseudoTagEntries;
/*178*/	LIST_ENTRY FreeLists[128];
/*578*/	LPCRITICAL_SECTION LockVariable;
/*57C*/	NTSTATUS (*CommitMemory)(struct _RTL_HEAP *, PVOID *, PDWORD);	/* XXX */
/*580*/	struct _RTL_HEAP_LOOKASIDE *Lookaside;
/*584*/	DWORD LookasideLockCount;
/*588*/	RTL_HEAP_UNCOMMITTED_RANGE UnCommittedRanges[8];
#if 0
/*608*/	/* An actual critical section structure is located here and pointed
	   to by CriticalSection above. */
/*620*/	/* Zero-filled gap resides here and is followed by the heap segment. */
#endif
} RTL_HEAP, *PRTL_HEAP;

/*
 * Heap segment flags.
 */
#define RTL_HEAP_SEGMENT_RETAIN		0x00000001

typedef struct _RTL_HEAP_SEGMENT {
/*000*/	struct _RTL_HEAP_ENTRY Entry;
/*008*/	DWORD Signature;
/*00C*/	DWORD Flags;
/*010*/	PRTL_HEAP Heap;
/*014*/	DWORD UnCommittedSize;
/*018*/	PVOID HeapBaseAddress;
/*01C*/	DWORD TotalPages;
/*020*/	PRTL_HEAP_ENTRY FirstBlock;
/*024*/	PRTL_HEAP_ENTRY LastBlock;
/*028*/	DWORD NumberOfUnCommittedPages;
/*02C*/	DWORD NumberOfUnCommittedRanges;
/*030*/	PRTL_HEAP_UNCOMMITTED_RANGE CurrentUnCommittedRange;
/*034*/	WORD AllocatorBackTraceIndex;
	WORD Unknown_036;
/*038*/	PRTL_HEAP_FREE_BLOCK FreeBlock;
	PVOID Unknown_03C;
} RTL_HEAP_SEGMENT, *PRTL_HEAP_SEGMENT;

/*
 * Heap lookaside buffer is used for blocks of which size is less than 128.
 */
typedef struct _RTL_HEAP_LOOKASIDE {
/*000*/	SLIST_HEADER Head;
/*008*/	WORD Depth;
/*00A*/	WORD MaxDepth;
/*00C*/	DWORD AllocateCount;
/*010*/	DWORD AllocateFailureCount;
/*014*/	DWORD FreeCount;
/*018*/	DWORD FreeFailureCount;
/*01C*/	DWORD PrevAllocateCount;
/*020*/	DWORD PrevAllocateFailureCount;
	DWORD Unknown_024;	/* XXX unused? */
	DWORD Unknown_028;	/* XXX unused? */
	DWORD Unknown_02C;	/* XXX unused? */
} RTL_HEAP_LOOKASIDE, *PRTL_HEAP_LOOKASIDE;

typedef struct _RTL_HEAP_INFORMATION {
/*000*/	DWORD Length;			/* sizeof(RTL_HEAP_INFORMATION) */
/*004*/	DWORD SegmentReserve;
/*008*/	DWORD SegmentCommit;
/*00C*/	DWORD DeCommitFreeBlockThreshold;
/*010*/	DWORD DeCommitTotalFreeThreshold;
/*014*/	DWORD MaximumAllocationSize;
/*018*/	DWORD VirtualMemoryThreshold;
/*01C*/	DWORD CommitSize;
/*020*/	DWORD ReserveSize;
/*024*/	NTSTATUS (*CommitMemory)(struct _RTL_HEAP *, PVOID *, PDWORD);	/* XXX */
/*028*/	DWORD Unknown_028;	/* XXX unused? */
/*02C*/	DWORD Unknown_02C;	/* XXX unused? */
} RTL_HEAP_INFORMATION, *PRTL_HEAP_INFORMATION;

typedef struct _RTL_PROCESS_HEAP_ENTRY {
/*000*/	PVOID Data;
/*004*/	DWORD DataLength;
/*008*/	BYTE Overhead;
/*009*/	BYTE Index;
/*00A*/	WORD Flags;
	union {
		struct {
/*00C*/			HANDLE MemoryHandle;
/*010*/			WORD Tag;
/*012*/			WORD Unknown_012;
/*014*/			DWORD Unknown_014;	/* XXX unused? */
/*018*/			DWORD Unknown_018;	/* XXX unused? */
		} Block;
		struct {
/*00C*/			DWORD CommittedSize;
/*010*/			DWORD UnCommittedSize;
/*014*/			LPVOID FirstBlock;
/*018*/			LPVOID LastBlock;
		} Segment;
	};
} RTL_PROCESS_HEAP_ENTRY, *PRTL_PROCESS_HEAP_ENTRY;

typedef struct _HEAP_HANDLE_ENTRY {
	union {
/*000*/		struct _HEAP_HANDLE_ENTRY *NextUnusedEntry;
		struct {
/*000*/			WORD Flags;
/*002*/			WORD LockCount;
		};
	};
/*004*/	PVOID Mem;
} HEAP_HANDLE_ENTRY, *PHEAP_HANDLE_ENTRY;

typedef struct _HEAP_HANDLE_TABLE {
/*000*/	DWORD BlockSize;		/* Divided by 8 */
/*004*/	DWORD EntrySize;		/* = sizeof(HEAP_HANDLE_ENTRY) */
	DWORD Unknown_008;
	DWORD Unknown_00C;
/*010*/	PHEAP_HANDLE_ENTRY FirstUnusedEntry;
/*014*/	PVOID BaseAddress;
/*018*/	PVOID LastAddress;
/*01C*/	PVOID MaximumAddress;
} HEAP_HANDLE_TABLE, *PHEAP_HANDLE_TABLE;

#endif /* !_HEAP_H */
