#	$Mogua: mogua.prog.mk,v 1.2 2003/01/02 08:00:18 junyoung Exp $

.include <mogua.init.mk>

clean:		cleanprog

PROG_EXE=	${PROG}.exe

.c.o:
	${COMPILE.c} ${.IMPSRC}

.if defined(PROG)
SRCS?=		${PROG}.c
.endif

OBJS+=	${SRCS:N*.h:N*.sh:R:S/$/.o/g}

${PROG_EXE}: ${OBJS}
	${CC} -o ${PROG_EXE} ${OBJS} ${LDLIBS}

realall: ${PROG_EXE} ${SRCS} ${OBJS}

cleanprog:
	rm -f ${PROG_EXE} ${OBJS}
