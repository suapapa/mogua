#	$Mogua: mogua.init.mk,v 1.2 2002/04/07 13:21:16 junyoung Exp $
#	$NetBSD: bsd.init.mk,v 1.1 2001/11/02 05:21:50 tv Exp $

# <mogua.init.mk> includes Makefile.inc and <mogua.own.mk>; this is used at the
# top of all <mogua.*.mk> files which actually "build something".

.if !target(__initialized__)
__initialized__:
.-include "${.CURDIR}/../Makefile.inc"
.include <mogua.own.mk>
.MAIN:		all
.endif
