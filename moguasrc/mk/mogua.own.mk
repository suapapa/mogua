#	$Mogua: mogua.own.mk,v 1.4 2002/10/17 12:10:40 junyoung Exp $

.if !defined(_MOGUA_OWN_MK_)
_MOGUA_OWN_MK_=1

MAKECONF?=	/etc/mogua.mk.conf
.-include "${MAKECONF}"

TOOLDIR?=	/usr/cross

USETOOLS?=	yes

.if ${USETOOLS} == "yes"

AR=		${TOOLDIR}/bin/${MACHINE_GNU_PLATFORM}-ar
AS=		${TOOLDIR}/bin/${MACHINE_GNU_PLATFORM}-as
DLLTOOL=	${TOOLDIR}/bin/${MACHINE_GNU_PLATFORM}-dlltool
DLLWRAP=	${TOOLDIR}/bin/${MACHINE_GNU_PLATFORM}-dllwrap
LD=		${TOOLDIR}/bin/${MACHINE_GNU_PLATFORM}-ld
NM=		${TOOLDIR}/bin/${MACHINE_GNU_PLATFORM}-nm
OBJCOPY=	${TOOLDIR}/bin/${MACHINE_GNU_PLATFORM}-objcopy
OBJDUMP=	${TOOLDIR}/bin/${MACHINE_GNU_PLATFORM}-objdump
RANLIB=		${TOOLDIR}/bin/${MACHINE_GNU_PLATFORM}-ranlib
SIZE=		${TOOLDIR}/bin/${MACHINE_GNU_PLATFORM}-size
STRIP=		${TOOLDIR}/bin/${MACHINE_GNU_PLATFORM}-strip
WINDRES=	${TOOLDIR}/bin/${MACHINE_GNU_PLATFORM}-windres

CC=		${TOOLDIR}/bin/${MACHINE_GNU_PLATFORM}-gcc
CPP=		${TOOLDIR}/bin/${MACHINE_GNU_PLATFORM}-cpp
CXX=		${TOOLDIR}/bin/${MACHINE_GNU_PLATFORM}-c++
FC=		${TOOLDIR}/bin/${MACHINE_GNU_PLATFORM}-g77
OBJC=		${TOOLDIR}/bin/${MACHINE_GNU_PLATFORM}-gcc

MACHINE_GNU_ARCH=${GNU_ARCH.${MACHINE_ARCH}:U${MACHINE_ARCH}}
MACHINE_GNU_PLATFORM=${MACHINE_GNU_ARCH}-mingw32

.endif		# USETOOLS == yes

all:	.NOTMAIN realall

# NetBSD sys.mk has -traditional-cpp defined, which is harmful to our
# mingw32 toolchain. So we override it here (not a good method, though).
COMPILE.S=	${CC} ${AFLAGS} ${CPPFLAGS} -c

.endif		# _MOGUA_OWN_MK_
