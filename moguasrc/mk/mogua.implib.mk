#	$Mogua: mogua.implib.mk,v 1.1 2003/03/25 06:18:44 junyoung Exp $

.include <mogua.init.mk>

clean:		cleanlib

IMPLIB=		lib${LIB}.a
DEF=		${LIB}.def

${IMPLIB}: ${DEF}
	${DLLTOOL} --as=${AS} -k --output-lib ${IMPLIB} --def ${DIST}/${DEF}

realall: ${IMPLIB}

cleanlib:
	rm -f ${IMPLIB}