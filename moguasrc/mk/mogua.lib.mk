#	$Mogua: mogua.lib.mk,v 1.10 2002/11/21 15:18:50 junyoung Exp $

.include <mogua.init.mk>

clean:		cleanlib
	
IMPLIB=		lib${LIB}.a
DYNLIB=		${LIB}.dll
DEF=		${LIB}.def

CPPFLAGS+=	-nostdinc
CXXFLAGS+=	-nostdinc++

.c.o:
	${COMPILE.c} ${.IMPSRC}

.cc.o .cxx.o .cpp.o:
	${COMPILE.cc} ${.IMPSRC}

.S.o .s.o:
	${COMPILE.S} ${CFLAGS:M-[ID]*} ${AINC} ${.IMPSRC} -o ${.TARGET}

_LIBS=	${IMPLIB} ${DYNLIB} ${DEF}

OBJS+=	${SRCS:N*.h:N*.sh:R:S/$/.o/g}

realall: ${SRCS} ${OBJS} ${_LIBS}

MAJOROSVER=	5
MINOROSVER=	0

DLLWRAPFLAGS=	--def ${DEF} \
		--image-base ${IBASE} --driver-name ${CC} \
		--driver-flags "-nostdlib \
			-Wl,--enable-stdcall-fixup \
			-Wl,--strip-debug \
			-Wl,--major-os-version,${MAJOROSVER} \
			-Wl,--minor-os-version,${MINOROSVER}"

${DYNLIB}: ${OBJS} ${DEF} ${IMPLIB}
	${DLLWRAP} ${DLLWRAPFLAGS} -o ${DYNLIB} ${OBJS} ${LDLIBS}
		
cleanlib:
	rm -f ${IMPLIB} ${DYNLIB} ${OBJS}
