/*
 * Copyright 2003
 *     Bang Jun-Young.  All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *	This product includes software developed by Bang Jun-Young.
 * 4. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <windows.h>
#include <ntdll.h>
#include <ddk/ntstatus.h>

HANDLE WINAPI
GetProcessHeap(void)
{
	PTEB Teb = NtCurrentTeb();

	return Teb->Peb->ProcessHeap;
}

DWORD WINAPI
GetProcessHeaps(DWORD NumberOfHeaps, PHANDLE ProcessHeaps)
{
	return RtlGetProcessHeaps(NumberOfHeaps, ProcessHeaps);
}

#if 0
PVOID WINAPI
HeapAlloc(HANDLE hHeap, DWORD dwFlags, SIZE_T dwBytes)
{
	/* Forwarded to NTDLL.RtlAllocateHeap */
}
#endif

SIZE_T WINAPI
HeapCompact(HANDLE hHeap, DWORD dwFlags)
{
	return RtlCompactHeap(hHeap, dwFlags);
}

HANDLE WINAPI
HeapCreate(DWORD flOptions, SIZE_T dwInitialSize, SIZE_T dwMaximumSize)
{
	HANDLE Handle;
	BOOL Growable;
	DWORD PageSize;

#if 0 /* XXX not yet ready for use... */
	PageSize = BaseStaticServerData->PageSize;
#else
	PageSize = 0x1000;
#endif
	Growable = FALSE;
	flOptions &= (HEAP_NO_SERIALIZE | HEAP_GENERATE_EXCEPTIONS);
	flOptions |= HEAP_CREATED_VIA_HEAP_CREATE;

	if (dwMaximumSize < PageSize) {
		if (dwMaximumSize == 0) {
			Growable = TRUE;
			flOptions |= HEAP_GROWABLE;
		} else
			dwMaximumSize = PageSize;
	}

	if (Growable == FALSE &&
	    dwInitialSize > dwMaximumSize)
		dwMaximumSize = dwInitialSize;

	Handle = RtlCreateHeap(flOptions, NULL, dwMaximumSize, dwInitialSize,
	    FALSE, NULL);
	if (Handle == NULL)
		SetLastError(ERROR_NOT_ENOUGH_MEMORY);

	return Handle;
}

BOOL WINAPI
HeapDestroy(HANDLE hHeap)
{
	if (RtlDestroyHeap(hHeap) != NULL) {
		SetLastError(ERROR_INVALID_HANDLE);
		return FALSE;
	}

	return TRUE;
}

#if 0
BOOL WINAPI
HeapFree(HANDLE hHeap, DWORD dwFlags, PVOID pMem)
{
	/* Forwarded to NTDLL.RtlFreeHeap */
}
#endif

BOOL WINAPI
HeapLock(HANDLE hHeap)
{
	return RtlLockHeap(hHeap);
}

#if 0
LPVOID WINAPI
HeapReAlloc(HANDLE hHeap, DWORD dwFlags, PVOID pMem, SIZE_T dwBytes)
{
	/* Forwarded to NTDLL.RtlReAllocateHeap */
}

SIZE_T WINAPI
HeapSize(HANDLE hHeap, DWORD dwFlags, PCVOID pMem)
{
	/* Forwarded to NTDLL.RtlSizeHeap */
}
#endif

BOOL WINAPI
HeapUnlock(HANDLE hHeap)
{
	return RtlUnlockHeap(hHeap);
}

BOOL WINAPI
HeapValidate(HANDLE hHeap, DWORD dwFlags, LPCVOID lpMem)
{
	return RtlValidateHeap(hHeap, dwFlags, lpMem);
}

BOOL WINAPI
HeapWalk(HANDLE hHeap, LPPROCESS_HEAP_ENTRY lpEntry)
{
	RTL_PROCESS_HEAP_ENTRY Entry;
	NTSTATUS Status;

	if (lpEntry->lpData == NULL)
		Entry.Data = NULL;
	else {
		Entry.Data = lpEntry->lpData;
		Entry.Index = lpEntry->iRegionIndex;
		if (lpEntry->wFlags & PROCESS_HEAP_REGION)
			Entry.Flags = RTL_PROCESS_HEAP_SEGMENT;
		else if (lpEntry->wFlags & PROCESS_HEAP_UNCOMMITTED_RANGE) {
			Entry.Flags = RTL_PROCESS_HEAP_UNCOMMITTED_RANGE;
			Entry.DataLength = lpEntry->cbData;
		} else
			/* PROCESS_HEAP_ENTRY_BUSY (0x0004) */
			Entry.Flags = (lpEntry->wFlags >> 2) & 1;
	}

	Status = RtlWalkHeap(hHeap, &Entry);
	if (Status < STATUS_SUCCESS) {
		SetLastStatus(Status);
		return FALSE;
	}

	lpEntry->lpData = Entry.Data;
	lpEntry->cbData = Entry.DataLength;
	lpEntry->cbOverhead = Entry.Overhead;
	lpEntry->iRegionIndex = Entry.Index;
	if (Entry.Flags & RTL_PROCESS_HEAP_ENTRY_BUSY) {
		lpEntry->wFlags = PROCESS_HEAP_ENTRY_BUSY;
		if (Entry.Flags & RTL_PROCESS_HEAP_ENTRY_DDESHARE)
			lpEntry->wFlags = PROCESS_HEAP_ENTRY_DDESHARE |
			    PROCESS_HEAP_ENTRY_BUSY;
		if (Entry.Flags & RTL_PROCESS_HEAP_ENTRY_MOVEABLE) {
			lpEntry->Block.hMem = Entry.Block.hMem;
			lpEntry->wFlags |= PROCESS_HEAP_ENTRY_MOVEABLE;
		}
		lpEntry->Block.dwReserved[0] = 0;
		lpEntry->Block.dwReserved[1] = 0;
		lpEntry->Block.dwReserved[2] = 0;
	} else if (Entry.Flags & RTL_PROCESS_HEAP_SEGMENT) {
		/* Segment is the NTDLL terminology corresponding to
		   KERNEL32 heap region. */
		lpEntry->wFlags = PROCESS_HEAP_REGION;
		lpEntry->Region.dwCommittedSize = Entry.Segment.CommittedSize;
		lpEntry->Region.dwUnCommittedSize =
		    Entry.Segment.UnCommittedSize;
		lpEntry->Region.lpFirstBlock = Entry.Segment.FirstBlock;
		lpEntry->Region.lpLastBlock = Entry.Segment.LastBlock;
	} else if (Entry.Flags & RTL_PROCESS_HEAP_UNCOMMITTED_RANGE) {
		lpEntry->wFlags = PROCESS_HEAP_UNCOMMITTED_RANGE;
		memset(&lpEntry->Block, 0, sizeof(lpEntry->Block));
	} else
		lpEntry->wFlags = 0;

	return TRUE;
}
