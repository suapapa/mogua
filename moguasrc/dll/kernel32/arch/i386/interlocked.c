/*
 * Copyright 2003
 *     Bang Jun-Young.  All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *	This product includes software developed by Bang Jun-Young.
 * 4. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <windows.h>

LONG WINAPI
InterlockedCompareExchange(LPLONG volatile Destination, LONG Exchange,
    LONG Comperand)
{
	LONG ReturnValue;

	__asm __volatile(
	    "lock cmpxchgl %0,(%1)"
	    : "=r" (ReturnValue)
	    : "r" (Destination), "r" (Exchange), "0" (Comperand)
	    : "memory");

	return ReturnValue;
}

LONG WINAPI
InterlockedDecrement(LPLONG volatile Addend)
{
	LONG ReturnValue;

	__asm __volatile(
	    "lock xaddl %0,(%1)\n\t"
	    "decl %0"
	    : "=a" (ReturnValue)
	    : "r" (Addend), "0" (-1)
	    : "memory");

	return ReturnValue;
}

LONG WINAPI
InterlockedExchange(LPLONG volatile Target, LONG Value)
{
	LONG ReturnValue;

	__asm __volatile(
	    "lock xchgl %0,(%1)"
	    : "=r" (ReturnValue)
	    : "r" (Target), "0" (Value)
	    : "memory");

	return ReturnValue;
}

LONG WINAPI
InterlockedExchangeAdd(LPLONG volatile Addend, LONG Value)
{
	LONG ReturnValue;

	__asm __volatile(
	    "lock xaddl %0,(%1)"
	    : "=r" (ReturnValue)
	    : "r" (Addend), "0" (Value)
	    : "memory");

	return ReturnValue;
}

LONG WINAPI
InterlockedIncrement(LPLONG volatile Addend)
{
	LONG ReturnValue;

	__asm __volatile(
	    "lock xaddl %0,(%1)\n\t"
	    "incl %0"
	    : "=a" (ReturnValue)
	    : "r" (Addend), "0" (1)
	    : "memory");

	return ReturnValue;
}
